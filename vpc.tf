resource "aws_vpc" "dev" {
  cidr_block= "${var.vpc_cidr}"

  enable_dns_support = "true" #gives you an internal domain name
  enable_dns_hostnames = "true" #gives you an internal host name

  tags = {
     Name = "${var.env}"
   }
}
resource "aws_internet_gateway" "IGW" {
  vpc_id = "${aws_vpc.dev.id}"

  tags = {
    Name = "IGW-${var.env}"
  }
}

output "vpc_id" {
  value = "${aws_vpc.dev.id}"
  }

output "IGW_id" {
  value = "${aws_internet_gateway.IGW.id}"
 }
