#variable "aws_access_key" {}
#variable "aws_secret_key" {}

variable "vpc_cidr"{
    default = "155.144.0.0/16"
}

variable "env" {}
variable "AWS_REGION" {
    default = "ap-south-1"
}

variable "Private_Subnet_cidr" {
    type = list(string)
    default = ["155.144.1.0/24","155.144.2.0/24","155.144.3.0/24"]
}

variable "Public_Subnet_cidr" {
    type = list(string)
    default = ["155.144.4.0/24","155.144.5.0/24","155.144.6.0/24"]
}


#variable "azs" {
#    type = list(string)
#    default = ["ap-south-1a","ap-south-1b","ap-south-1c"]
#}

# Declare the data source
data "aws_availability_zones" "azs" {
  state = "available"
}

variable "name_security_groups" {
  default = "my-sg"
}
