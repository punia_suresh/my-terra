# The default provider configuration; resources that begin with `aws_` will use
# it as the default, and it can be referenced as `aws`.
provider "aws" {
  region = "${var.AWS_REGION}"
  #region = "ap-south-1"
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}

# Additional provider configuration for west coast region; resources can
# reference this as `aws.singapore`.
#provider "aws" {
#  alias  = "singapore"
#  region = "ap-southeast-1"
#}
